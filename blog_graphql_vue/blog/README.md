# Run it locally

    composer install
    npm install
    npm run watch
    cp .env.example .env
    configurar .env
    php artisan migrate
    php artisan serve

# http://localhost:8000/graphql-playground


# WEB SPA  Single Page Application

Concepto:

Es una pagina la cual todo el contenido esta dentro de un solo archivo, solo se carga el contenido HTML de una sola pagina y todo el contenido de la pagina se encuetra ahi

 * Son mas rapidas porque el contenido html,css y js se cargue solo una vez.
 * Tenemos varias vistas mas no, varias paginas.
 * Podemos tener una sola pagina y muchas vistas que serian los apartados de la pagina web.
 * Cambia la url conforme se va moviendo por la pagina sustituyendo el contenido
*  SPA siempre estan hechas en JS, se ejecuta unicamente en el lado del cliente, esto porque en los navegadores web solo podemos ejecutar el JS
* Frameworks

    angular
    react
    vue

# Backend

No tiene mucha relevancia en que fue desarrollado, se puede utilizar cualquier herramienta, solo dependen de una api que le muestre el contenido a mostar.


# Herramientas de desarrollo 

## Vue js


* Guia de instalacion y utilidad

    https://vuejs.org/v2/guide/


## Node js
* Guia de instalacion y utilidad

    https://nodejs.org/es/


## Tres caracteriscas mencionadas en el video
->  1 

    Tenemos varias vistas mas no, varias paginas, todo se maneja desde vue, donde seria el fronted y se manejan todas las vistas.

![imagen](img/uno.png)


-> 2

    Cambia la url conforme se va moviendo por la pagina sustituyendo el contenido, pero tambien se puede generar de forma que cambie solo por vista, se crea un solo controlador que sea el de spa, para cuando se hace la ruta solo se tiene una sola, que se dirige a ese unico controlador

![imagen](img/dos.png)

->3

    Son mas rapidas porque el contenido html,css y js se cargue solo una vez. Por el desarrollo que tienen las mismas, esto seria un ejemplo de como se contruye una vista, donde solo se carga lo necesario, que seria el contenido de la pagina, con un poco de html, css y js

![imagen](img/tres.png)

    Una de las ventajas es que inpendientemente de lo que haga la url sigue siendo la misma, por lo que no hay necesidad de generar lineas de rutas.
    Ejemplo de que independientemente de la solicitud esta se mantiene igual

En el caso de realizar un nuevo registro:

![imagen](img/cuatro.png)

Cuando se elimina un registro y recarga el contenido, se sigue manteniendo la URL
![imagen](img/siete.png)
![imagen](img/cinco.png)















